#!/bin/bash

MOPIDY_CONF=/config/mopidy.conf
if test -f "$MOPIDY_CONF"; then
    echo "$MOPIDY_CONF exist"
else
    echo "init mopidy"
    cp /etc/default/mopidy.conf $MOPIDY_CONF
    mkdir /var/lib/mopidy/media/local
    mkdir /var/lib/mopidy/media/playlists
fi

mopidy local scan --limit 50

exec "$@"