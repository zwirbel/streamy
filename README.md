# streamy
Aggregates music from various stream sources, records and re-broadcasts on the fly.  
Uses Mopidy as frontend to manage and forward audio streams to the web radio / Icecast.  

Stream Sources -> Mopidy -> Icecast -> Streamripper -> Disk  

## Services

| Service | URL | Purpose | 
|---|---|---|
| Iris | http://localhost:6680/iris | Streaming frontend with audio output configured to Icecast |
| Icecast | http://localhost:8000/stream | Re-broadcasts incoming stream |
| Streamripper | - | Rips stream from Icecast to disk | 
| Media Files | http://localhost:8080 | View local media and ripped tracks |
| MPD | http://localhost:6600 | Remote control |
| Party | http://localhost:6680/party | Search and add tracks to queue | 

## Stream Sources
Following sources are currently available (some might need additional configuration):  

- local
- HTTP
- Spotify
- Soundcloud
- YouTube
  
Optionally (can be enabled in mopidy/Dockerfile):
- Google Music
- DLNA
- SomaFM
- RadioNET
- InternetArchives
  
## Run locally
```
git clone https://gitlab.com/0x1d/streamy.git && cd streamy && mkdir config && cp mopidy/etc/mopidy.conf && cp icecast/etc/icecast.xml config
docker-compose up
```
Go to http://localhost:6680/iris and queue up some tracks.  
In case you've enabled Spotify, you need to authenticate in `Settings`.
To listen to the stream, open http://localhost:8000/stream in your media player.  
Altough Mopidy supports listening through the browser, it might not work in some setups.  

## Run in the Cloud
Given you're connected with a K8s cluster, just...
```
kubectl apply -f -R install 
```

## Configuration
To stream from Spotify and Soundcloud, authenticate and generate API tokens here: https://mopidy.com/authenticate/  
Update config/mopidy.conf accordingly.  
### Defaults
See service/etc directories.

## TODO
- more K8s instructions