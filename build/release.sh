#!/bin/bash

function release_tag {
    git tag -fa $1 -m "$2"
    git push origin master --tags
}

function promote_image {
    docker tag $1 $2
    docker push $2
}