function docker_login() {
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY
}

function compose_build() {
    docker-compose build
}

function compose_push() {
    docker-compose build
}

function build_image() {
    local TAG=$DOCKER_REPO/$3:$1
    local DOCKERFILE=$2
    local CONTEXT=$3
    docker build -t $TAG -f $CONTEXT/$DOCKERFILE $CONTEXT
}

function push_image() {
    docker push $DOCKER_REPO/$2:$1
}

function release_image() {
    local TAG=$DOCKER_REPO/$3:$1
    local TAG_RELEASE=$DOCKER_REPO/$3:$2

    docker pull $TAG
    docker tag $TAG $TAG_RELEASE
    docker push $TAG_RELEASE
}
